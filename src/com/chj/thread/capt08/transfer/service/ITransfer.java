package com.chj.thread.capt08.transfer.service;

import com.chj.thread.capt08.transfer.UserAccount;

/**
 *	银行转账动作接口
 */
public interface ITransfer {
    void transfer(UserAccount from, UserAccount to, int amount)
    		throws InterruptedException;
}
