package com.chj.thread.capt08.transfer;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 用户账户的实体类
 * @author Administrator
 *
 */
public class UserAccount {
	  ///private int id;
    private final String name;//账户名称
    private int money;//账户余额

    private final Lock lock = new ReentrantLock();

    public Lock getLock() {
        return lock;
    }

    public UserAccount(String name, int amount) {
        this.name = name;
        this.money = amount;
    }

    /*
     * 转入资金
     */
    public void addMoney(int amount) {
    	money = money + amount;
    }
    
    /*
     * 转出资金
     */
    public void reduceMoney(int amount){
        money = money - amount;
    }
    
    public String getName() {
        return name;
    }

    public int getAmount() {
        return money;
    }
    
    @Override
    public String toString() {
        return "UserAccount{" +
                "name='" + name + '\'' +
                ", money=" + money +
                '}';
    }
    
    
}
