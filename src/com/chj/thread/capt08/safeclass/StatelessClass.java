package com.chj.thread.capt08.safeclass;

/**
 * 没有任何成员变量的类，就叫无状态的类，这种类一定是线程安全的。
 * 如果这个类的方法参数中使用了对象，也是线程安全的吗？比如：
 * 
 * 当然也是，因为多线程下的使用，固然user这个对象的实例会不正常，但是对于StatelessClass这个类的对象实例来说，
 * 它并不持有UserVo的对象实例，它自己并不会有问题，有问题的是UserVo这个类，而非StatelessClass本身。
 * @author Administrator
 *
 */
public class StatelessClass {
	
	public int service(int a,int b){
	    return a+b;
    }

    public void serviceUser(UserVo user){
        //do sth user
    }

}
