package com.chj.thread.capt08.safeclass;

import java.util.ArrayList;
import java.util.List;

/**
 * 类不可变--事实不可变
 * 根本就不提供任何可供修改成员变量的地方，同时成员变量也不作为方法的返回值
 * @author Administrator
 *
 */
public class ImmutableClassToo {
	
	private final List<Integer> list = new ArrayList<>(3);
	
	public ImmutableClassToo() {
		list.add(1);
	    list.add(2);
	    list.add(3);
	}

	public boolean isContain(int i){
		return list.contains(i);
	 }
}
