package com.chj.thread.capt08.safepublish;

import java.util.ArrayList;
import java.util.List;

/**
 * 但是如果类中持有的成员变量是对象的引用，如果这个成员对象不是线程安全的，通过get等方法发布出去，
 * 会造成这个成员对象本身持有的数据在多线程下不正确的修改，从而造成整个类线程不安全的问题
 * 
 * 这个list发布出去后，是可以被外部线程之间修改，那么在多个线程同时修改的情况下不安全问题是肯定存在的，
 * 怎么修正这个问题呢？我们在发布这对象出去的时候，就应该用线程安全的方式包装这个对象。
 * 参见代码cn.enjoyedu.ch7.safeclass. SafePublishToo
 * 
 */
public class UnSafePublish {
	private List<Integer> list = new ArrayList<>(3);
	
    public UnSafePublish() {
    	list.add(1);
    	list.add(2);
    	list.add(3);
    }
	
	public List getList() {
		return list;
	}

	public static void main(String[] args) {
		UnSafePublish unSafePublish = new UnSafePublish();
		List<Integer> list = unSafePublish.getList();
		System.out.println(list);
		list.add(4);
		System.out.println(list);
		System.out.println(unSafePublish.getList());
	}
}
