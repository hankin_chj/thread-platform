package com.chj.thread.capt08.singleton;
/**
 * 	懒汉式-双重检查
 * @author Administrator
 *
 */
public class SingletonDoubleCheck {
	
	private volatile static SingletonDoubleCheck sDoubleCheck;

	// 私有化
	private SingletonDoubleCheck() {}
	
	public static SingletonDoubleCheck getInstance() {
		//第一次检查，不加锁
		if(sDoubleCheck == null) {
			System.out.println(Thread.currentThread()+" is null");
			synchronized(SingletonDoubleCheck.class) {
				//第二次检查，加锁情况下
				if(sDoubleCheck == null) {
					 System.out.println(Thread.currentThread()+" is null");
	                 //内存中分配空间  1
	                 //空间初始化 2
	                 //把这个空间的地址给我们的引用  3
					sDoubleCheck = new SingletonDoubleCheck();
				}
			}
		}
		return sDoubleCheck;
	}

}
