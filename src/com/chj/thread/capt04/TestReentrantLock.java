package com.chj.thread.capt04;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.chj.thread.tools.SleepTools;

/**
 * ReentrantLock不仅能实现和synchronized相同的功能，还具有嗅探锁定、多路分支通知等功能，而且使用上也比synchronized灵活
 * @author Administrator
 * 
 * getHoldCount()：查询当前线程保持锁定的个数，也就是调用lock方法的次数
 * getQueueLength()：返回正在等待获取此锁定的线程估计个数
 * getWaitQueueLength()：返回等待与此锁定相关的给定条件的Condition的线程估计数（调用condition的await方法的线程个数）
 * isFair(): 作用时判断是不是公平锁（ReentrantLock默认情况使用的时非公平锁）
 * isHeldByCurrentThread（）：查询当前线程是否保持锁定
 * tryLock()：仅在调用时锁定 未被另一个线程保持的情况下，才获取该锁定
 * 
 * 
 *
 */
public class TestReentrantLock {

	/**
	 * 	可重入锁的实现主要包含三个要素：
	 * 	原子状态： 使用CAS操作 来存储当前的锁的状态，判断锁是否被别的线程持有了
	 * 	等待队列： 所有没有请求到锁的线程，会进入等待队列进行等待，待有线程释放锁以后，系统就能从队列中唤醒一个线程，继续工作。
	 * 	阻塞：park() unpark()，用来挂起恢复线程
	 */
	 public void test() {
		 // ReentrantLock(boolean fair) 通过fair 可以使用公平锁，非公平锁
		 final Lock lock = new ReentrantLock();
	        
        class Worker extends Thread {
            public void run() {
                while (true) {
                	// 调用该方法代码的线程就持有了“对象监视器”，其他线程只有在等候锁被释放时再次争抢
                    lock.lock();
                    try {
                    	SleepTools.second(1);
                        System.out.println(Thread.currentThread().getName());
                        SleepTools.second(1);
                    } finally {
                        lock.unlock();
                    }
                    SleepTools.second(2);
                }
            }
        }
        // 启动10个子线程
        for (int i = 0; i < 10; i++) {
            Worker w = new Worker();
            w.setDaemon(true);
            w.start();
        }
        // 主线程每隔1秒换行
        for (int i = 0; i < 10; i++) {
        	SleepTools.second(1);
            System.out.println("========="+i);
        }
    }
	 
	public static void main(String[] args) {
		  TestReentrantLock testMyLock = new TestReentrantLock();
	      testMyLock.test();

	}

}
