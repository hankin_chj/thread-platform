package com.chj.thread.capt09;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.chj.thread.capt09.vo.ITaskProcesser;
import com.chj.thread.capt09.vo.JobInfo;
import com.chj.thread.capt09.vo.TaskResult;
import com.chj.thread.capt09.vo.TaskResultType;

/**
 * 	框架的主体类，也是调用者主要使用的类
 * @author Administrator
 *
 */
public class PendingJobPool {
	
	// 框架运行时的线程数，与机器的CPU数相同
	private static final int THREAD_COUNTS = Runtime.getRuntime().availableProcessors();
	
	// 用于存放任务的队列，供线程池使用
	private static BlockingQueue<Runnable> taskQueue = new ArrayBlockingQueue<Runnable>(5000);
	
	// 线程池，固定大小，有界队列
	private static ExecutorService taskExcutor = new ThreadPoolExecutor(
			THREAD_COUNTS, THREAD_COUNTS, 60, TimeUnit.SECONDS, taskQueue);
	
	// 工作信息的存储容器
	private static ConcurrentHashMap<String,JobInfo<?>> jobInfoMap = new ConcurrentHashMap<String,JobInfo<?>>();
	
//	/*检查过期工作的处理器*/
//	private static CheckJobProcesser checkJob = CheckJobProcesser.getInstance();
	
	/**
	 * 
	 * @return
	 */
	public static Map<String, JobInfo<?>> getMap() {
		return jobInfoMap;
	}


	// 单列模式启动
	private PendingJobPool() {}

	private static class PendingJobPoolHolder{
		private static PendingJobPool pool = new PendingJobPool();
	}
	
	public static PendingJobPool getInstance() {
		return PendingJobPoolHolder.pool;
	}
	
	/**
	 * 对工作中的任务进行包装，提交给线程池使用，并将处理任务的结果，写入缓存以供查询
	 * @author Administrator
	 */
	private static class PendingTask<T,R> implements Runnable{
		
		private JobInfo<R> jobInfo;
		private T processData;
		
		public PendingTask(JobInfo<R> jobInfo, T processData) {
			super();
			this.jobInfo = jobInfo;
			this.processData = processData;
		}

		@Override
		public void run() {
			R r = null;
			ITaskProcesser<T,R> taskProcesser = (ITaskProcesser<T,R>)jobInfo.getTaskProcesser();
			TaskResult<R> result = null;
			try {
				result = taskProcesser.executeTask(processData);
				if(result == null) {
					result = new TaskResult<R>(TaskResultType.exception,r,"result is null");
				}
				if(result.getResultType() == null) {
					if(result.getReason()==null){
						result = new TaskResult<R>(TaskResultType.exception,r,"result is null");
					}else{
						result = new TaskResult<R>(TaskResultType.exception,r
								,"result is null,reason:"+result.getReason());
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
				result = new TaskResult<R>(TaskResultType.exception,r,e.getMessage());
			}finally {
				// 每个任务处理完成后，记录任务的处理结果
				jobInfo.addTaskResult(result);
			}
		}
	}
	
	/**
	 * 调用者提交工作中的任务
	 * @param jobName
	 * @param t
	 */
	public <T,R> void putTask(String jobName,T t) {
		JobInfo<R> jobInfo = getJob(jobName);
		PendingTask<T,R> task = new PendingTask<>(jobInfo, t);
		taskExcutor.execute(task);
	}
	
	/**
	 * 调用者注册工作，如工作名，任务的处理器等等
	 */
	public <R> void registerJob(String jobName,int jobSize,
			ITaskProcesser<?,?> taskProcesser, long expireTime) {
		
		JobInfo<R> jobInfo = new JobInfo<R>(jobName,jobSize,taskProcesser,expireTime);
		if(jobInfoMap.putIfAbsent(jobName, jobInfo) != null) {
			throw new RuntimeException(jobName+"已经注册！");
		}
	}
	
	/**
	 * 获得工作的整体处理进度
	 * @param jobName
	 * @return
	 */
	public <R> String getTaskProcess(String jobName) {
		JobInfo<R> jobInfo = getJob(jobName);
		return jobInfo.getTotalProcess();
	}
	
	/**
	 * 获得每个任务的处理详情
	 * @param jobName
	 * @return
	 */
	public <R> List<TaskResult<R>> getTaskDetail(String jobName){
		JobInfo<R> jobInfo = getJob(jobName);
		return jobInfo.getTaskDetail();
	}

	/**
	 * 根据工作名臣检索工作
	 * @param jobName
	 * @return
	 */
	private <R> JobInfo<R> getJob(String jobName) {
		JobInfo<R> jobInfo = (JobInfo<R>)jobInfoMap.get(jobName);
		if(null == jobInfo) {
			throw new RuntimeException(jobName + "是非法任务！");
		}
		return jobInfo;
	}
	
	
	
	
	
}
