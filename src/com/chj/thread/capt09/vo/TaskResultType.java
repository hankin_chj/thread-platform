package com.chj.thread.capt09.vo;

public enum TaskResultType {
	
	success,/*方法执行完成，业务结果也正确*/
	failure,/*方法执行完成，业务结果错误*/
	exception/*方法执行抛出了异常*/

}
