package com.chj.thread.capt09;

import java.util.List;
import java.util.Random;

import com.chj.thread.capt09.vo.TaskResult;
import com.chj.thread.tools.SleepTools;

/**
 * 模拟一个应用程序，提交工作和任务，并查询任务进度
 * @author Administrator
 *
 */
public class JobPoolTest {

	private final static String JOB_NAME = "计算数值";
	private final static int JOB_LENGTH = 1000;
	
	/**
	 * 	查询任务进度的线程
	 */
	private static class QueryResult implements Runnable{
		
		private PendingJobPool pool;
		
		public QueryResult(PendingJobPool pool) {
			super();
			this.pool = pool;
		}

		@Override
		public void run() {
			int i = 0;
			while(i < 350) {
				List<TaskResult<String>> taskDetails = pool.getTaskDetail(JOB_NAME);
				if(!taskDetails.isEmpty()) {
					System.out.println(pool.getTaskProcess(JOB_NAME));
					System.out.println(taskDetails);
				}
				SleepTools.ms(100);
				i++;
			}
		}
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		MyTask myTask = new MyTask();
		PendingJobPool pool = PendingJobPool.getInstance();
		//
		pool.registerJob(JOB_NAME, JOB_LENGTH, myTask, 5);
		Random r = new Random();
		for(int i=0; i<JOB_LENGTH; i++) {
			pool.putTask(JOB_NAME, r.nextInt(1000));
		}
		
		Thread t = new Thread(new QueryResult(pool));
		t.start();
	}
	
}
