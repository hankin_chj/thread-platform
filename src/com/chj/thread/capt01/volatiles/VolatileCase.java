package com.chj.thread.capt01.volatiles;

import com.chj.thread.tools.SleepTools;

/**
 * 	演示Volatile的提供的可见性：volatile是线程得轻量级实现，并且只能修饰变量
 * 	强制从公共堆栈中取得变量得值，而不是从线程私有数据栈中取得变量得值
 *  volatile解决得是变量在多个线程之间得可见性，而synchronized关键字解决得是多个线程之间访问资源得同步性
 * @author Administrator
 *
 */
public class VolatileCase {
	
	//volatile保证了不同线程对这个变量进行操作时的可见性，即一个线程修改了某个变量的值，这新值对其他线程来说是立即可见的。
	private volatile static boolean ready;
	
	// 启动线程时，该变量存在于公共堆栈 及线程得私有堆栈中，线程一直在私有堆栈中取得ready为true，
	// 代码ready = false; 虽然被执行，更新得却是公共堆栈中得ready变量值false,所以一直时死循环
//	private static boolean ready = true;
	
	private static int number;
	
	private static class PrintThread extends Thread{
		@Override
        public void run() {
            System.out.println("PrintThread is running.......");
            while(ready) {};//无限循环
            System.out.println("线程被终止了，number = "+number);
        }
	}

	public static void main(String[] args) {
		new PrintThread().start();
		SleepTools.second(1);
        number = 51;
        // 主线程修改ready 如果使用volatile修饰该变量则子线程在修改后会感知到修改结果
        ready = false;
        SleepTools.second(5);
        System.out.println("main is ended!");

	}

}
