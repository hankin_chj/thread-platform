package com.chj.thread.capt01.volatiles;

public class NotSafe {
	
	private volatile long count =0;

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    //count进行累加
//    public synchronized void incCount(){
    public void incCount(){
    	// i++操作不是原子性得， 该操作实际上分为三个步骤：
    	// 1. 从内存中获取i得值。 2.计算i得值。 3.将i得值写入内存中
        count++;
    }

    //线程
    private static class Count extends Thread{

        private NotSafe simplOper;

        public Count(NotSafe simplOper) {
            this.simplOper = simplOper;
        }

        @Override
        public void run() {
            for(int i=0;i<10000;i++){
                simplOper.incCount();
            }
        }
    }
    

	public static void main(String[] args) throws InterruptedException {
		 NotSafe simplOper = new NotSafe();
        //启动两个线程
        Count count1 = new Count(simplOper);
        Count count2 = new Count(simplOper);
        count1.start();
        count2.start();
        Thread.sleep(50);
        System.out.println(simplOper.count);//20000?
	}

}
