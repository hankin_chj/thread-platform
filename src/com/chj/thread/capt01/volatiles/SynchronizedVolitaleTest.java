package com.chj.thread.capt01.volatiles;

public class SynchronizedVolitaleTest {
	
	private boolean isContinue = true;
	
	public void runMethod() {
		while(isContinue) {
			// 如果没有synchronized关键字，得到结果是无法打印停止运行，主要原因是线程间没有可见性造成的
			// 而synchronized关键字在多个线程访问同一个资源的时候具有同步性，具有可视性；
			// 而且它还具有将线程工作内存中的私有变量与公共内存中的变量同步的功能。
			synchronized(this){
				//
			}
		}
		System.out.println("停止运行。。。。");
	}
	
	public void stopMethod() {
		isContinue = false;
	}

	static class ThreadA extends Thread{
		private SynchronizedVolitaleTest test;
		public ThreadA(SynchronizedVolitaleTest test) {
			super();
			this.test = test;
		}

		@Override
        public void run() {
			test.runMethod();
		}
	}

	// 
	static class ThreadB extends Thread{
		private SynchronizedVolitaleTest test;
		
		public ThreadB(SynchronizedVolitaleTest test) {
			super();
			this.test = test;
		}

		@Override
        public void run() {
			test.stopMethod();
		}
	}
	public static void main(String[] args) throws InterruptedException {
		// 
		SynchronizedVolitaleTest test = new SynchronizedVolitaleTest();
		new ThreadA(test).start();
		Thread.sleep(1000);
		new ThreadB(test).start();
		System.out.println("发起停止命令！！！");

	}

}
