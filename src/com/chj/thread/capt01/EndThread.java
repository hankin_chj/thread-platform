package com.chj.thread.capt01;

public class EndThread {
	
	private static class UseThread extends Thread{
		public UseThread(String name) {
			super(name);
		}
		
		@Override
		public void run() {
			String threadName = Thread.currentThread().getName();
//			while(true()) {// 子线程永远不会中断
			while(!isInterrupted()) {// 子线程会中断
				System.out.println("thread----"+threadName+" is run ----");
			}
			System.out.println(threadName+" interrupt flag =="+isInterrupted());
		}
		
	}
	

	public static void main(String[] args) throws InterruptedException {
		Thread enThread = new UseThread("enThread");
		enThread.start();
		Thread.sleep(20);
		// 休眠20毫秒以后中断该线程
		enThread.interrupt();
		

	}

}
