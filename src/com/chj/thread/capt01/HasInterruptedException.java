package com.chj.thread.capt01;

public class HasInterruptedException {
	
	private static class UseThread extends Thread{
		public UseThread(String name) {
			super(name);
		}
		
		@Override
		public void run() {
			String threadName = Thread.currentThread().getName();
//			while(true()) {// 子线程永远不会中断
			// 判断线程是否需要中断
//			while(!Thread.currentThread().isInterrupted()) {
			while(!isInterrupted()) {// 子线程会中断
				try {
//					System.out.println("thread----"+threadName+" is run ----");
					Thread.sleep(100);
				} catch (InterruptedException e) {
					System.out.println(threadName+"catch interrupt flag =="+isInterrupted());
					// 如果方法里面里面有InterruptedException异常，外面对用interrupt()方法时候不会中断
					// 如果有异常：isInterrupted会改变为flase, 所有子线程会一直执行
//					interrupt();
					
					e.printStackTrace();
				}
				System.out.println(threadName+"while interrupt flag =="+isInterrupted());
			}
			System.out.println(threadName+" interrupt flag =="+isInterrupted());
		}
		
	}
	

	public static void main(String[] args) throws InterruptedException {
		Thread enThread = new UseThread("HasInterruptedException");
		// 
		enThread.start();
		Thread.sleep(500);
		// 休眠500毫秒以后 尝试中断（不会强制中断，设置中断标志）
		enThread.interrupt();
		

	}

}
