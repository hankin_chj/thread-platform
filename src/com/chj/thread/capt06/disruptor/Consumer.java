package com.chj.thread.capt06.disruptor;

import com.lmax.disruptor.WorkHandler;

/**
 * 消费者实现类
 * @author Administrator
 *
 */
public class Consumer implements WorkHandler<PCData>{

	@Override
	public void onEvent(PCData event) throws Exception {
		//
		System.out.println(Thread.currentThread().getId()+": Event: --" +
				event.getValue() * event.getValue() +"--");
		
	}

}
