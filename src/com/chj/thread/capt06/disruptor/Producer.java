package com.chj.thread.capt06.disruptor;

import java.nio.ByteBuffer;

import com.lmax.disruptor.RingBuffer;

/**
 * 
 * @author Administrator
 *
 */
public class Producer {
	
	private final RingBuffer<PCData> ringBuffer;

	public Producer(RingBuffer<PCData> ringBuffer) {
		this.ringBuffer = ringBuffer;
	}
	
	public void pushData(ByteBuffer bbf) {
		long sequence = ringBuffer.next();
		try{
			PCData event = ringBuffer.get(sequence);
			event.setValue(bbf.getLong(0));
		}finally {
			ringBuffer.publish(sequence);
		}
		
		
	}
	
	

}
