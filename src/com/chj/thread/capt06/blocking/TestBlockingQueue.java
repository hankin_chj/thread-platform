package com.chj.thread.capt06.blocking;

import java.util.Random;

public class TestBlockingQueue {
	
	/**
	 * 
	 */
	private static class ThreadPut implements Runnable{
		
		private IBoundedBuffer<String> boundedBuffer;
		
		public ThreadPut(IBoundedBuffer<String> boundedBuffer) {
			this.boundedBuffer = boundedBuffer;
		}
		
		@Override
		public void run() {
			 System.out.println("PutThread is running.....");
	         Random r1 = new Random();
//	         Random r2 = new Random();
	         for(int i =0;i<20;i++){
	        	 int number = r1.nextInt()+ 10;
	        	 try {
//	        		Thread.sleep(r2.nextInt(2000)+500);
					boundedBuffer.put("count["+i+"] number:"+number);
					System.out.println("count["+i+"] put number:"+number);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
	         }
		}
	}
	
	/**
	 * 
	 */
	private static class TakeThread implements Runnable{
		private String t_name;
        private IBoundedBuffer<String> boundedBuffer;

        public TakeThread(IBoundedBuffer<String> boundedBuffer, String t_name) {
            this.boundedBuffer = boundedBuffer;
            this.t_name = t_name;
        }

		@Override
		public void run() {
			Thread.currentThread().setName(t_name);
			System.out.println(t_name+" is running.....");
            Random r2 = new Random();
            while(!Thread.currentThread().isInterrupted()) {
            	try {
					String msg = boundedBuffer.take();
					System.out.println(t_name+" get "+msg);
	                Thread.sleep(r2.nextInt(2000)+500);
				} catch (InterruptedException e) {
					System.out.println(t_name+"���жϣ�");
					e.printStackTrace();
				}
            }
		}
	 }
	

	public static void main(String[] args) throws InterruptedException {
		// 
		IBoundedBuffer<String> boundedBuffer1 = new WaitNotifyImpl<>(10);
		// 
		IBoundedBuffer<String> boundedBuffer2 = new SemphoreImpl<>(10);
		//
		IBoundedBuffer<String> boundedBuffer3 = new LockConditionImpl<>(10);
		//
		IBoundedBuffer<String> boundedBuffer = new LoopSleepImpl<>(10);
		
		Thread put1 = new Thread(new ThreadPut(boundedBuffer));
		Thread take1 = new Thread(new TakeThread(boundedBuffer,"Thead_take1"));
		Thread take2 = new Thread(new TakeThread(boundedBuffer,"Thead_take2"));
		put1.start();
		Thread.sleep(2000);
		take1.start();
		take2.start();

	}

}
