package com.chj.thread.capt06.blocking;

/**
 * 
 * @author Administrator
 *
 * @param <T>
 */
public interface IBoundedBuffer<T> {
	
	void put(T t) throws InterruptedException;
	T take()  throws InterruptedException;

}
