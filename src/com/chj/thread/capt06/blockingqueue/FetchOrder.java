package com.chj.thread.capt06.blockingqueue;

import java.util.concurrent.DelayQueue;

/**
 * 取出到期的订单的功能
 * @author Administrator
 *
 */
public class FetchOrder implements Runnable{

	private DelayQueue<ItemVo<Order>> queue;

	public FetchOrder(DelayQueue<ItemVo<Order>> queue) {
		super();
		this.queue = queue;
	}

	@Override
	public void run() {
		while(true) {
			try {
				ItemVo<Order> item = queue.take();
				Order order = (Order)item.getData();
				System.out.println("Get From Queue:"+"data="+order.getOrderNo()+";"+order.getOrderMoney());
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
}
