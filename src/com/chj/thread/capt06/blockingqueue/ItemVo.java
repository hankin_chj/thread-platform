package com.chj.thread.capt06.blockingqueue;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * 	存放的队列的元素
 * @author Administrator
 *
 */
public class ItemVo<T> implements Delayed{

	// 到期时间但传入的数值代表过期时长，传入单位毫秒
	private long activeTime;
	// 业务数据，泛型
	private T data;
	
	public ItemVo(long activeTime, T data) {
		super();
		// 将传入的时长转换为超时的时刻
		this.activeTime = TimeUnit.NANOSECONDS.convert(activeTime, TimeUnit.MILLISECONDS)+System.nanoTime();
		this.data = data;
	}

	public long getActiveTime() {
		return activeTime;
	}

	public T getData() {
		return data;
	}
	
	/*
	 * Delayed接口继承了Comparable接口，按剩余时间排序，实际计算考虑精度为纳秒数
	 */
	@Override
	public int compareTo(Delayed o) {
		//
		long d = getDelay(TimeUnit.NANOSECONDS) - o.getDelay(TimeUnit.NANOSECONDS);
		return (d ==0) ? 0 : (d > 0 ? 1 : -1);
	}

	/*
	 * 这个方法返回到激活日期的剩余时间，时间单位由单位参数指定。(non-Javadoc)
	 */
	@Override
	public long getDelay(TimeUnit unit) {
		long delay = unit.convert(this.activeTime-System.nanoTime(), TimeUnit.NANOSECONDS);
		return delay;
	}

	

	
}
