package com.chj.thread.other.tree;


/**
 * 二叉查找树的实现：
 * @author Administrator
 *
 * @param <E>
 */
public class BinarySearchTree<E extends Comparable<? super E>> {

	private BinaryNode<E> root;
	
	public BinarySearchTree(BinaryNode<E> root) {
		this.root = root;
	}

	public void makeEmpty(){
        this.root = null;
    }
    
    public boolean isEmpty(){
        return this.root == null;
    }
    
    public boolean contains(E ele){
        return contains(ele, root);
    }


	public E findMax() throws Exception {
        if(isEmpty()){
            throw new Exception();
        }
        
        return findMax(root).element;
        
    }
 
	public E findMin() throws Exception {
        if(isEmpty()){
            throw new Exception();
        }
        
        return findMin(root).element;
    }
    
    public void insert(E ele){
        root = insert(ele,root);
    }


	public void remove(E e){
       root = remove(e, root);
    }
    
    public void printTree(){
        if(isEmpty()){
            System.out.println("empty tree");
        }else{
 
            printTree(root);
        }
    }
 
    private void printTree(BinaryNode<E> root) {
        if(null!=root){
            //使用的中序遍历
            printTree(root.left);
            System.out.println(root.element);
            printTree(root.right);
        }
    }

    
    /**
     * 
     * @param ele
     * @param root
     * @return
     */
    private BinaryNode<E> insert(E ele, BinaryNode<E> root) {
		if(null == root) {
			return new BinaryNode<E>(ele);
		}
		int compareResult = ele.compareTo(root.element); 
		if(compareResult < 0) {
			root.left = insert(ele,root.left);
		}else if(compareResult > 0) {
			root.right = insert(ele,root.right);
		}else {
			//
		}
		return root;
	}
    
    /**
     * 
     * @param root
     * @return
     */
	private BinaryNode<E> findMin(BinaryNode<E> root) {
		if(null != root) {
			while(null != root.left) {
				root = root.left;
			}
		}
		return root;
	}
    
	/**
	 * 查找最大节点
	 * @param root
	 * @return
	 */
    private BinaryNode<E> findMax(BinaryNode<E> root) {
		if(root != null) {
			while(null != root.right) {
				root = root.right;
			}
		}
		return root;
	}


    private boolean contains(E ele, BinaryNode<E> root) {
    	if(null == root) {
			return false;
		}
    	int compareResult = ele.compareTo(root.element);
    	if(compareResult < 0) {
    		return contains(ele,root.left);
		}else if(compareResult > 0) {
			return contains(ele,root.right);
		}else {
			return true;
		}
	}
    
    /**
     * 
     * @param e
     * @param root
     * @return
     */
    private BinaryNode<E> remove(E e, BinaryNode<E> root){
		if(null == root) {
			return root;
		}
		int compareResult = e.compareTo(root.element);
		if(compareResult < 0) {
			root.left = remove(e,root.left);
		}else if(compareResult > 0) {
			root.right = remove(e, root.right);
		}else if(null!=root.left && null!=root.right) {
			root.element = findMin(root.right).element;
			root.right = remove(root.element, root.right);
		}else {
			root = (null != root.left) ? root.left : root.right;
		}
		return root;
    	
    }



	/**
	 *	 内部类
     */
    private static class BinaryNode<E>{
    	
    	 E element;
         BinaryNode<E> left;
         BinaryNode<E> right;

		public BinaryNode(E element) {
			this(element, null, null);
		}

		public BinaryNode(E element, BinaryNode<E> left, BinaryNode<E> right) {
			this.element = element;
            this.left = left;
            this.right = right;
		}
    	
    	
    }
}
