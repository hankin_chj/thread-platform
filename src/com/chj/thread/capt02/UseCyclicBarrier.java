package com.chj.thread.capt02;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CyclicBarrier;

/**
 * 	循环栅栏
 * 
 * @author Administrator
 *
 */
public class UseCyclicBarrier {
	// 两个参数：第一个表示计数总数，第二个 barrierAction就是计数器完成一次计数后，系统会执行的动作
	private static CyclicBarrier barrier = new CyclicBarrier(5, new CollecThread());
	
	//存放子线程工作结果的容器
	private static ConcurrentHashMap<String, Long> resultMap = new ConcurrentHashMap<>();

	public static void main(String[] args) {
		 for(int i=0; i<5; i++){
            Thread thread = new Thread(new SubThread());
            thread.start();
        }
	}
	
	// 负责屏障开放以后的工作
	private static class CollecThread implements Runnable{

		@Override
		public void run() {
			StringBuilder result = new StringBuilder();
			for(Map.Entry<String,Long> workResult : resultMap.entrySet()) {
				result.append("["+workResult.getValue()+"]");
			}
			System.out.println(" the result = "+ result);
            System.out.println("do other business........");
		}
	}
	
	 //工作线程
    private static class SubThread implements Runnable{

		@Override
		public void run() {
			long id = Thread.currentThread().getId();//线程本身的处理结果
            resultMap.put(Thread.currentThread().getId()+"",id);
            Random r = new Random();//随机决定工作线程的是否睡眠
            try {
            	if(r.nextBoolean()) {
            		Thread.sleep(1000+id);
            		System.out.println("Thread_"+id+" ....do something------------ ");
            	}
            	System.out.println(id+"....is await.....");
            	barrier.await();
            	Thread.sleep(1000+id);
                System.out.println("Thread_"+id+" ....do its business!!!!! ");
            	
			} catch (Exception e) {
				e.printStackTrace();
			}
            	
         }
    	
    }

}
