package com.chj.thread.capt02.wn;

import com.chj.thread.tools.SleepTools;

/**
 * 《wait/notify实现生产者和消费者程序》
 * 采用多线程技术，例如wait/notify，设计实现一个符合生产者和消费者问题的程序，对某一个对象（枪膛）进行操作，
 * 其最大容量是20颗子弹，生产者线程是一个压入线程，它不断向枪膛中压入子弹，消费者线程是一个射出线程，它不断从枪膛中射出子弹。
 * @author Administrator
 *
 */
public class TestWaitNotify {

	/* 快递运输里程数 */
	private int bulletCount = 0;

	public TestWaitNotify() {}

	public TestWaitNotify(int bulletCount) {
		this.bulletCount = bulletCount;
	}
	
	// 弹出子弹方法
	public synchronized void popMethod() {
		while (this.bulletCount > 0) {
			this.bulletCount--;
			System.out.println("popMethod--: the bullet is poping for:" + this.bulletCount);
			//其他业务处理
			notifyAll();
		}
		
	}

	// 压入子弹方法，最大容量20个，超过进入等待
	public synchronized void pushMethod() {
		while (this.bulletCount >= 20) {
			try {
				wait();
				System.out.println("thread[" + Thread.currentThread().getId() + "]"
						+ " overdue max count and waiting Consumer Thread pop the bullet!.");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.bulletCount++;
		System.out.println("pushMethod++: the bullet is push in for:" + this.bulletCount);
		
	}

	
	private static TestWaitNotify gun = new TestWaitNotify();
	
	// 生产者线程
	private static class Producer extends Thread{
		@Override
        public void run() {
			gun.pushMethod();
		}
	}

	// 消费者线程
	private static class Consumer extends Thread{
		@Override
        public void run() {
			gun.popMethod();
		}
	}
	
	
	public static void main(String[] args) throws InterruptedException {
		// 启动生产者线程
		for(int i=0;i<50;i++){
			new Producer().start();
			SleepTools.ms(2);
		}
        
		// 启动消费者线程
		for(int i=0;i<50;i++){
			new Consumer().start();
			SleepTools.ms(2);
		}
		SleepTools.ms(2000);
	}
	

}
