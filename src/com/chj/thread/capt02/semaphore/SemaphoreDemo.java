package com.chj.thread.capt02.semaphore;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * 	从广义上说：信号量就是对锁的拓展 
 * @author Administrator
 *
 */
public class SemaphoreDemo implements Runnable{

	// 声明包含5个包含许可的信号量（即5个线程可以进入）
	final Semaphore semp = new Semaphore(5);

	@Override
	public void run() {
		//
		try {
			semp.acquire();
			Thread.sleep(2000);
			System.out.println("当前线程 "+Thread.currentThread().getName()+": done!!");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			semp.release();
		}
		
	}
	

	public static void main(String[] args) {
		//
		ExecutorService extService = Executors.newFixedThreadPool(20);
		final SemaphoreDemo demo = new SemaphoreDemo();
		for(int i=0; i<20; i++) {
			extService.submit(demo);
		}

	}

}
