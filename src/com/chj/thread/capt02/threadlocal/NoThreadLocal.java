package com.chj.thread.capt02.threadlocal;

public class NoThreadLocal {
	
	 static Integer count = new Integer(1);
	
	/**
	 * 	运行3个线程
	 */
	public void StartThreadArry() {
		Thread[] runs = new Thread[3];
		for (int i = 0; i < runs.length; i++) {
			runs[i] = new Thread(new TestThread(i));
		}
		for (int i = 0; i < runs.length; i++) {
			runs[i].start();
		}
	}
	
	//测试线程，线程的工作是将ThreadLocal变量的值变化，并写回，看看线程之间是否会互相影响
	public static class TestThread implements Runnable{

		int id;
		public TestThread(int id) {
			this.id = id;
		}

		@Override
		public void run() {
			 System.out.println(Thread.currentThread().getName()+":start");
			 count = count + id;
			 System.out.println(Thread.currentThread().getName()+":" +count);
			
		}
	}
	
	
	public static void main(String[] args) {
		NoThreadLocal test = new NoThreadLocal();
        test.StartThreadArry();
	}

}
