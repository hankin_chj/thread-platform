package com.chj.thread.capt10.bussiness;

/**
 * 用sleep模拟实际的业务操作
 * @author Administrator
 */
public class SL_Busi {
	
	public static void business(int sleepTime) {
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
