package com.chj.thread.capt10;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.chj.thread.capt10.bussiness.Consts;
import com.chj.thread.capt10.bussiness.CreatePendingDocs;
import com.chj.thread.capt10.bussiness.SL_QuestionBank;
import com.chj.thread.capt10.service.ProduceDocService;
import com.chj.thread.capt10.vo.SrcDocVo;

/**
 * 服务化，异步化
 * @author Administrator
 *
 */
public class RpcServiceWebV1 {
	// 处理文档生成的线程池 IO密集型 故而大小设置为CPU核心数*2
	private static ExecutorService docMakeService = Executors.newFixedThreadPool(Consts.THREAD_COUNT*2);
	
	// 处理文档上传的线程池
	private static ExecutorService docUploadService = Executors.newFixedThreadPool(Consts.THREAD_COUNT*2);
	// 
	private static CompletionService<String> docCompletingServcie = new ExecutorCompletionService(docMakeService);
	//
	private static CompletionService<String> docUploadCompletingServcie = new ExecutorCompletionService<String>(docUploadService);	

	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		int docCount = 60;
		 System.out.println("题库开始初始化...........");
	     SL_QuestionBank.initBank();
	     System.out.println("题库初始化完成。");
	     List<SrcDocVo> docList = CreatePendingDocs.makePendingDoc(docCount);
	     long startTotal = System.currentTimeMillis();
	     for(SrcDocVo doc : docList) {
	    	 docCompletingServcie.submit(new MakeDocTask(doc));
	     }
	     for(int i=0; i<docCount; i++) {
	    	Future<String> future  = docCompletingServcie.take();
	    	docUploadCompletingServcie.submit(new UploadTask(future.get()));
	     }
	     // 展示时间
	     for(int i=0; i<docCount; i++) {
	    	 docUploadCompletingServcie.take().get();
	     }
	     System.out.println("共耗时："+(System.currentTimeMillis()-startTotal)+"ms");

	}
	
	/**
	 * 生成文档的工作任务
	 * @author Administrator
	 *
	 */
	private static class MakeDocTask implements Callable<String>{

		private SrcDocVo pendingDocVo;
		
		public MakeDocTask(SrcDocVo pendingDocVo) {
			this.pendingDocVo = pendingDocVo;
		}

		@Override
		public String call() throws Exception {
			long start = System.currentTimeMillis();
			// 普通生成方式
			String result1 = ProduceDocService.makeDoc(pendingDocVo);
			// 题目并行化方式
			String result = ProduceDocService.makeDocAsyn(pendingDocVo);
			System.out.println("文档"+result+"生成耗时："+(System.currentTimeMillis()-start)+"ms");
	        return result;
		}
		
	}
	
	/**
	 * 上传文档的工作任务
	 * @author Administrator
	 *
	 */
	private static class UploadTask implements Callable<String>{

		private String fileName;

		public UploadTask(String fileName) {
			this.fileName = fileName;
		}

		@Override
		public String call() throws Exception {
			long start = System.currentTimeMillis();
			String result = ProduceDocService.upLoadDoc(fileName);
			System.out.println("已上传至[" + result + "]耗时：" + (System.currentTimeMillis() - start) + "ms");
			return result;
		}
		
	}
	
	
}
