package com.chj.thread.capt10.service;

import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import com.chj.thread.capt10.bussiness.SL_QuestionBank;
import com.chj.thread.capt10.vo.QuestionInCacheVo;
import com.chj.thread.capt10.vo.QuestionInDBVo;
import com.chj.thread.capt10.vo.TaskResultVo;

/**
 * 
 * @author Administrator
 *
 */
public class ParalleQstService {
	
	// 题目在本地的缓存
	private static ConcurrentHashMap<Integer,QuestionInCacheVo> questionCache = new ConcurrentHashMap<>();

	// 正在处理的题目的缓存
	private static ConcurrentHashMap<Integer,Future<QuestionInCacheVo>> processingQestionCache = new ConcurrentHashMap<>();
	
	// 处理题目的线程池
	private static ExecutorService makeQuestionExector = Executors.newCachedThreadPool();

	
	/**
	 * 
	 * @param questionId
	 * @return
	 */
	public static TaskResultVo makeQuestion(Integer questionId) {
		// 
		QuestionInCacheVo questionInCacheVo = questionCache.get(questionId);
		if(null == questionInCacheVo) {
			 System.out.println("题目["+questionId+"]不存在，准备启动");
			 return new TaskResultVo(getQuestionFuture(questionId));
		}else {
			String questionSha = SL_QuestionBank.getQuestionSha(questionId);
			if(questionInCacheVo.getQuestionSha().equals(questionSha)) {
				System.out.println("题目["+questionId+"]在缓存已存在，可以使用");
				return new TaskResultVo(questionInCacheVo.getQuestionDetail());
			}else {
				System.out.println("题目["+questionId+"]在缓存已过期，准备更新");
                return new TaskResultVo(getQuestionFuture(questionId));
			}
			
		}
	}

	/**
	 * 获取题目任务
	 * @param questionId
	 * @return
	 */
	private static Future<QuestionInCacheVo> getQuestionFuture(Integer questionId) {
		Future<QuestionInCacheVo> questionFuture = processingQestionCache.get(questionId);
		try {
			if(null == questionFuture) {
				QuestionInDBVo questionInDBVo = SL_QuestionBank.getQuetion(questionId);
				//
				QuestionTask questionTask = new QuestionTask(questionInDBVo,questionId);
				//不靠谱的做法，无法避免两个线程对同一个题目进行处理
//                questionFuture = makeQuestionExecutor.submit(questionTask);
//                processingQuestionCache.putIfAbsent(questionId,questionFuture);
                // 如果直接改成
//                processingQuestionCache.putIfAbsent(questionId,questionFuture);
//                questionFuture = makeQuestionExecutor.submit(questionTask);
                // 也不行，因为ConcurrentHashMap的value是不允许为null的，那么就需要另做处理
				FutureTask<QuestionInCacheVo> ftask = new FutureTask<>(questionTask);
				questionFuture = processingQestionCache.putIfAbsent(questionId, ftask);
				if(null == questionFuture) {
					//当前线程成功了占位了
					questionFuture = ftask;
					makeQuestionExector.execute(ftask);
					System.out.println("当前任务已启动，请等待完成后");
				}else {
					System.out.println("有其他线程开启了题目的计算任务，本任务无需开启");
				}
			}else {
				System.out.println("当前已经有了题目的计算任务，不必重复开启");
			}
			return questionFuture;
		}catch(Exception e) {
			processingQestionCache.remove(questionId);
			e.printStackTrace();
	        throw e;
		}
	}
	
	/**
	 * 解析题目的任务类，调用最基础的题目生成服务即可
	 * @author Administrator
	 *
	 */
	private static class QuestionTask implements Callable<QuestionInCacheVo> {

		QuestionInDBVo questionDBVo;
        Integer questionId;

        public QuestionTask(QuestionInDBVo questionDBVo, Integer questionId) {
            this.questionDBVo = questionDBVo;
            this.questionId = questionId;
        }
        
		@Override
		public QuestionInCacheVo call() throws Exception {
			try {
				String questionDetail = QstService.makeQuestion(questionId, questionDBVo.getDetail());
				String questionSha = questionDBVo.getSha();
				QuestionInCacheVo questionInCacheVo = new QuestionInCacheVo(questionDetail,questionSha);
				return questionInCacheVo;
			}finally {
				//无论正常还是异常，均要将生成题目的任务从缓存中移除
				processingQestionCache.remove(questionId);
			}
			
		}
		
	}

}
