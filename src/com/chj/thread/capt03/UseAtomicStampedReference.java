package com.chj.thread.capt03;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * 演示带版本戳的原子操作类
 * @author Administrator
 *
 */
public class UseAtomicStampedReference {
	
	static AtomicStampedReference<String> asr = new AtomicStampedReference<String>("hankin",0);

	public static void main(String[] args) throws InterruptedException {
		// 初始的版本号
		final int oldStamp = asr.getStamp();
		final String oldReference = asr.getReference();
		System.out.println("oldReference=="+oldReference+",   oldStamp==="+oldStamp);
		
		Thread rightStampThread = new Thread(new Runnable() {
			@Override
			public void run() {
				boolean  result = asr.compareAndSet(oldReference, oldReference+"+java", oldStamp, oldStamp+1);
				System.out.println(Thread.currentThread().getName()
						+" 当前变量值："+oldReference+" 当前版本戳："+oldStamp+", result=="+result);
			}
		});
		
		Thread errorStampThread = new Thread(new Runnable() {
			@Override
			public void run() {
				String reference = asr.getReference();
				boolean result = asr.compareAndSet(reference, reference+"+C语言 ",oldStamp, oldStamp+1);
				System.out.println(Thread.currentThread().getName()
						+" 当前变量值："+reference+"; 当前版本戳："+asr.getStamp()+"-result=="+result);
			}
		});
		
		rightStampThread.start();
		rightStampThread.join();
		errorStampThread.start();
		errorStampThread.join();
		System.out.println("oldReference=="+oldReference+",   oldStamp==="+oldStamp);
		
	}

}
