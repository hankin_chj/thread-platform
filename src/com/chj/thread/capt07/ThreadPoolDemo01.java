package com.chj.thread.capt07;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolDemo01 {

	public static void main(String[] args) {
//		LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>(2);
//		RejectedExecutionHandler handler = new ThreadPoolExecutor.DiscardPolicy();
//		
//		 ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 10, 60, TimeUnit.MILLISECONDS,queue,handler);
          
		 ExecutorService pool = Executors.newFixedThreadPool(2);
         for(int i=1;i<=20;i++){
             MyTask myTask = new MyTask(i);
             pool.execute(myTask);
             System.out.println("线程池中线程数目："+i);
//             executor.getQueue().size()+"，已执行玩别的任务数目："+executor.getCompletedTaskCount());
         }
         pool.shutdown(); //关闭线程池
        
 
	}
	
	//
//	ExecutorService pool = Executors.newFixedThreadPool(2);

}

class MyTask implements Runnable {
	private int taskNum=1;
    
	public MyTask(int num) {
	    this.taskNum = num;
	}
	    
	@Override
	 public void run(){
		System.out.println("正在执行task "+taskNum);
		try{
			Thread.sleep(5000);
		}catch (InterruptedException e){
	      e.printStackTrace();
	    }
		System.out.println("task "+taskNum+"执行完毕");
	}
}
