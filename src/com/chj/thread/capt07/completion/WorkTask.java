package com.chj.thread.capt07.completion;

import java.util.Random;
import java.util.concurrent.Callable;

public class WorkTask implements Callable<Integer> {
	
	String name;
	
	public WorkTask(String name) {
		super();
		this.name = name;
	}


	@Override
	public Integer call() {
		int sleeptime = new Random().nextInt(1000);
		try {
			Thread.sleep(sleeptime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return sleeptime;
	}

}
