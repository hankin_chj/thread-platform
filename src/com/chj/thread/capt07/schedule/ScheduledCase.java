package com.chj.thread.capt07.schedule;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 	演示 ScheduledThreadPoolExecutor 的用法
 * @author Administrator
 *
 */
public class ScheduledCase {

	public static void main(String[] args) {
		// 
		ScheduledThreadPoolExecutor schedule = new ScheduledThreadPoolExecutor(2);
		// 提交固定时间间隔的任务
		schedule.scheduleAtFixedRate(new ScheduleWorker(ScheduleWorker.HasException), 
				1000, 2000, TimeUnit.MILLISECONDS);
		
		schedule.scheduleAtFixedRate(new ScheduleWorker(ScheduleWorker.Normal), 
    			1000, 2000, TimeUnit.MILLISECONDS);
		
		
	}

}
