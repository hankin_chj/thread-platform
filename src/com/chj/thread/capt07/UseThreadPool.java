package com.chj.thread.capt07;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.chj.thread.tools.SleepTools;

/**
 * 	线程池的使用
 * @author Administrator
 *
 */
public class UseThreadPool {
	
	/*
	 *  1）corePoolSize：表示核心线程池的大小。当提交一个任务时，如果当前核心线程池的线程个数小于corePoolSize，则会创建新的线程来执行所提交的
	 *  	任务，即使当前核心线程池有空闲的线程。如果当前核心线程池的线程个数等于corePoolSize，则不再重新创建线程，这个任务就会保存到
	 *  	BlockingQueue，如果调用prestartAllCoreThreads() 方法就会一次性的启动corePoolSize 个数的线程。
	 *  2）maximumPoolSize：表示线程池能创建线程的最大个数。如果当阻塞队列BlockingQueue已满时，
	 *  	并且当前线程池线程个数没有超过maximumPoolSize的话，就会创建新的线程来执行任务。
	 *  3）keepAliveTime：空闲线程存活时间。如果当前线程池的线程个数已经超过了corePoolSize，并且线程空闲时间超过了keepAliveTime的话，
	 *  	就会将这些空闲线程销毁，这样可以尽可能降低系统资源消耗。
	 *  4）unit：时间单位，为keepAliveTime指定时间单位。
	 *  5）workQueue：阻塞队列，用于保存任务的阻塞队列，可以使用ArrayBlockingQueue, LinkedBlockingQueue, SynchronousQueue, PriorityBlockingQueue。
	 *  6）threadFactory：创建线程的工程类。可以通过指定线程工厂为每个创建出来的线程设置更有意义的名字，如果出现并发问题，也方便查找问题原因。
	 *  7）handler：饱和策略，当线程池的阻塞队列已满和指定的线程都已经开启，说明当前线程池已经处于饱和状态了，那么就需要采用一种策略来处理这种情况。采用的策略有这几种： 
	 *  	AbortPolicy： 直接拒绝所提交的任务，并抛出RejectedExecutionException异常；
	 *  	CallerRunsPolicy：只用调用者所在的线程来执行任务；
	 *  	DiscardPolicy：不处理直接丢弃掉任务；
	 *  	DiscardOldestPolicy：丢弃掉阻塞队列中存放时间最久的任务，执行当前任务
	 */
    public static void main(String[] args) throws InterruptedException, ExecutionException{
    	// 使用ThreadPoolExecutor
    	ExecutorService pool0 = new ThreadPoolExecutor(2,5,3,TimeUnit.SECONDS,
    			new ArrayBlockingQueue<Runnable>(2),
    			new ThreadPoolExecutor.DiscardOldestPolicy());
    	
    	/*
    	 * 适用于为了满足资源管理的需求，而需要限制当前线程数量的应用场景，它适用于负载比较重的服务器。
    	 * FixedThreadPool的corePoolSize和maximumPoolSize都被设置为创建FixedThreadPool时指定的参数nThreads。
    	 * 当线程池中的线程数大于corePoolSize时，keepAliveTime为多余的空闲线程等待新任务的最长时间，超过这个时间后多余的线程将被终止。
    	 * 这里把keepAliveTime设置为0L，意味着多余的空闲线程会被立即终止。
    	 * FixedThreadPool使用有界队列LinkedBlockingQueue作为线程池的工作队列（队列的容量为Integer.MAX_VALUE）。
    	 */
    	ExecutorService pool1 = Executors.newFixedThreadPool(2); 
    	
    	/*
    	 * 创建使用单个线程的SingleThread-Executor的API，用于需要保证顺序地执行各个任务；并且在任意时间点，不会有多个线程是活动的应用场景。
    	 * corePoolSize和maximumPoolSize被设置为1，其他参数与FixedThreadPool相同。
    	 * SingleThreadExecutor使用有界队列LinkedBlockingQueue作为线程池的工作队列（队列的容量为Integer.MAX_VALUE
    	 */
    	ExecutorService pool2 = Executors.newSingleThreadExecutor(); 
    	
    	/*
    	 * 大小无界的线程池，适用于执行很多的短期异步任务的小程序，或者是负载较轻的服务器。
    	 * CachedThreadPool使用没有容量的SynchronousQueue作为线程池的工作队列，但CachedThreadPool的maximumPool是无界的
    	 * 如果主线程提交任务的速度高于maximumPool中线程处理任务的速度时，CachedThreadPool会不断创建新线程。
    	 * 极端情况下，CachedThreadPool会因为创建过多线程而耗尽CPU和内存资源。
    	 */
    	ExecutorService pool3 = Executors.newCachedThreadPool();
    	
    	//利用所有运行的处理器数目来创建一个工作窃取的线程池，使用fork-join实现
    	ExecutorService pool4 = Executors.newWorkStealingPool();
    	
    	// 包含若干个线程的ScheduledThreadPoolExecutor。适用于需要多个后台线程执行周期任务，同时为了满足资源管理的需求而需要限制后台线程的数量的应用场景。
    	ExecutorService pool5 = Executors.newScheduledThreadPool(2);
    	
    	// 适用于需要单个后台线程执行周期任务，同时需要保证顺序地执行各个任务的应用场景。
    	ExecutorService pool = Executors.newSingleThreadScheduledExecutor();
    	
    	
    	
    	for(int i=0; i<10; i++) {
    		Worker worker = new Worker("worker_"+i);
    		pool.execute(worker);
    	}
    	
    	// 调用又返回结果的方法执行任务
    	for(int i=0; i<10; i++) {
    		CallWorker callworker = new CallWorker("callWorker_"+i);
    		Future<String>  result = pool.submit(callworker);
    		System.out.println("任务返回结果 "+result.get());
    	}
    	pool.shutdown();
    }
    
	
	/**
	 * 	工作线程
	 */
	static class Worker implements Runnable{
		private String taskName;
        private Random r = new Random();

        public Worker(String taskName){
            this.taskName = taskName;
        }

        public String getName() {
            return taskName;
        }
		@Override
		public void run() {
			 System.out.println(Thread.currentThread().getName()
	            		+" process the task : " + taskName);
	         SleepTools.ms(r.nextInt(100)*5);
		}
	}
	
	/**
	 * 有返回值的工作线程
	 */
	static class CallWorker implements Callable<String>{

        private String taskName;
        private Random r = new Random();

        public CallWorker(String taskName){
            this.taskName = taskName;
        }

        public String getName() {
            return taskName;
        }   
        
		@Override
		public String call() throws Exception {
			 System.out.println(Thread.currentThread().getName()
	            		+" 处理任务: " + taskName);
	         return "result: "+Thread.currentThread().getName()+":"+r.nextInt(100)*5;
		}
	}
	

}
