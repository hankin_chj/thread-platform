package com.chj.thread.capt05.bitconvert;

/**
 * 	演示位运算
 * @author Administrator
 *
 */
public class IntToBinary {
	
	public static void main(String[] args) {
		int data = 4;
		System.out.println("the 4 binary is:"+Integer.toBinaryString(data));
		System.out.println("the 6 binary is "+Integer.toBinaryString(6));
		
		// 位与  &(1&1=1 1&0=0 0&0=0)
		System.out.println("the 4&6 is "+Integer.toBinaryString(4&6));
		
		// 位或 | (1|1=1 1|0=1 0|0=0)
		System.out.println("the 4|6 is "+Integer.toBinaryString(4|6));
		
		//位非~（~1=0  ~0=1）
    	System.out.println("the ~4 is "+Integer.toBinaryString(~4));
    	
    	System.out.println("------------------------------------------------------");
    	
    	//位异或 ^ (1^1=0 1^0=1 0^0=0)
    	System.out.println("the 4^6 is " + Integer.toBinaryString(4^6));
		// 有符号右移>>(若正数,高位补0,负数,高位补1)
		System.out.println("the 4>>1 is : " + Integer.toBinaryString(4 >> 1));
		// 有符号左移<<(若正数,高位补0,负数,高位补1)
		System.out.println("the 4<<1 is : " + Integer.toBinaryString(4 << 1));
		
		// 无符号右移>>>(不论正负,高位均补0)
		System.out.println("the 234567 is : " + Integer.toBinaryString(234567));
		System.out.println("the 234567>>>4 is : " + Integer.toBinaryString(234567 >>> 4));
		
		// 无符号右移>>>(不论正负,高位均补0)
		System.out.println("the -4 is : " + Integer.toBinaryString(-4));
		System.out.println("the -4>>>4 is : " + Integer.toBinaryString(-4 >>> 4));
		System.out.println(Integer.parseInt(Integer.toBinaryString(-4 >>> 4), 2));
    	
    	// <<有符号左移 >>有符号的右移  >>>无符号右移
    	//取模的操作 a % (2^n) 等价于 a&(2^n-1)
    	System.out.println("the 345 % 16 is: "+(345%16)+ " or :"+(345&(16-1)));
    	
    	// 
    	System.out.println("------------------------------------------------------");
    	System.out.println(Integer.parseInt("0001111", 2) & 15);
    	 
    	System.out.println(Integer.parseInt("0011111", 2) & 15);
    	 
    	System.out.println(Integer.parseInt("0111111", 2) & 15);
    	 
    	System.out.println(Integer.parseInt("1111111", 2) & 15);

    	 System.out.println("Mark hashCode is : "+"Mark".hashCode()+"="
         		+Integer.toBinaryString("Mark".hashCode()));
    	 
         System.out.println("Bill hashCode is : "+"Bill".hashCode()+"="
         		+Integer.toBinaryString("Bill".hashCode()));        
	}
	
	
	// hash算法
    private static int hash(Object k) {

    	int h = 13;
        h ^= k.hashCode();

        h += (h <<  15) ^ 0xffffcd7d;
        h ^= (h >>> 10);
        h += (h <<   3);
        h ^= (h >>>  6);
        h += (h <<   2) + (h << 14);
        return h ^ (h >>> 16);
    }

}
