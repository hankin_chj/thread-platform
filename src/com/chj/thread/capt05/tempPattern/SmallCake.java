package com.chj.thread.capt05.tempPattern;

/**
 *	 小蛋�?
 */
public class SmallCake extends AbstractCake {

    private boolean flag = false;
    public void setFlag(boolean shouldApply){
        flag = shouldApply;
    }
    @Override
    protected boolean shouldApply() {
        return this.flag;
    }
    
    @Override
    protected void shape() {
        System.out.println("小蛋糕�?�型");
    }

    @Override
    protected void apply() {
        System.out.println("小蛋糕涂�?");
    }

    @Override
    protected void brake() {
        System.out.println("小蛋糕烘�?");
    }

}
